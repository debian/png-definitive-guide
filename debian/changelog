png-definitive-guide (20060430-4) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.
  * debian/control: set 'Multi-Arch: foreign'.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 23 Aug 2021 20:39:33 -0300

png-definitive-guide (20060430-3) experimental; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/contact.html: created to tell how to contact the author. See
    debian/rules for details.
  * debian/control:
      - Added a Homepage field.
      - Added jdupes to Build-Depends field.
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Added Vcs-* fields.
      - Bumped Standards-Version to 4.5.1.
      - Improved long description.
      - Updated Recommends field to use firefox-esr instead of iceweasel, no
        longer available in Debian.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Updated all data.
  * debian/png-definitive-guide.doc-base: renamed to doc-base.
  * debian/png-definitive-guide.install: renamed to install.
  * debian/rules:
      - override_dh_install: created to change an external link by a link to
        contact.html to avoid a possible security hole.
      - override_dh_installexamples: removed because there are no examples to be
        installed.
      - override_dh_link: created to remove duplicate files.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/source/lintian-overrides: created to override a false positive about
    invariant sections in GFDL license used by upstream.
  * debian/upstream/metadata: created.
  * debian/watch: bumped version to 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 11 Jul 2021 22:47:51 -0300

png-definitive-guide (20060430-2) unstable; urgency=medium

  * QA upload.
  * Switch to dh.
  * Switch to "3.0 (quilt)" source format.
  * Package is orphaned (Bug #817762), set Maintainer to "Debian QA Group".
  * Raise debhelper compat level to 9. Closes: #817622.

 -- Santiago Vila <sanvila@debian.org>  Sun, 28 Aug 2016 20:24:02 +0200

png-definitive-guide (20060430-1) unstable; urgency=low

  * New upstream release
  * debian/watch: added
  * Refer to common GFDL
  * Fix build-depends
  * Update doc-base section
  * Change recommends to iceweasel

 -- Ross Burton <ross@debian.org>  Wed, 02 Jul 2008 09:19:04 +0100

png-definitive-guide (20030726-2) unstable; urgency=low

  * Add Recommends on www-browser (closes: #260131)

 -- Ross Burton <ross@debian.org>  Sun, 23 Jan 2005 18:21:34 +0000

png-definitive-guide (20030726-1) unstable; urgency=low

  * Initial Release.

 -- Ross Burton <ross@debian.org>  Thu,  5 Feb 2004 17:18:25 +0000
